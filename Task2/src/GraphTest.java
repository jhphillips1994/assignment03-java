import javafx.application.Application;
import javafx.application.Platform;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GraphTest extends Application {
	  private String text = "";
	  private double height = 0.0;
	
	@Override
	   public void start(Stage primaryStage) throws Exception {
	 
	       CategoryAxis xAxis = new CategoryAxis();
	       NumberAxis yAxis = new NumberAxis();
	       // Create a BarChart
	       BarChart<String, Number> barChart = new BarChart<String, Number>(xAxis, yAxis);
	       // Series 1 - Data of 2014
	       XYChart.Series<String, Number> dataSeries1 = new XYChart.Series<String, Number>();
	       
	       Data<String, Number> first = new XYChart.Data<String, Number>("1", height);
	       dataSeries1.getData().add(first);
	 
	 
	       // Add Series to BarChart.
	       barChart.getData().add(dataSeries1);
	       barChart.setTitle("Some Programming Languages");
	       
	        new Thread(new Runnable() {
	            @Override
	            public void run() {
	              try {
	                while (true) {
	                  if (barChart.getTitle().trim().length() == 0) {
	                	  text = "Welcome";
	                	  height += 1;
	                  } else
	                      text = "";
	                  	
	                  Platform.runLater(new Runnable() {
	                    @Override 
	                    public void run() {
	                    	barChart.setTitle(text);
	                    	Data<String, Number> second = new XYChart.Data<String, Number>("1", height + 1);
	                    	dataSeries1.getData().add(second);
	                    }
	                  });
	                  
	                  Thread.sleep(200);
	                }
	              }
	              catch (InterruptedException ex) {
	              }
	            }
	          }).start();
	 
	       VBox vbox = new VBox(barChart);
	 
	       primaryStage.setTitle("JavaFX BarChart (o7planning.org)");
	       Scene scene = new Scene(vbox, 400, 200);
	 
	       primaryStage.setScene(scene);
	       primaryStage.setHeight(300);
	       primaryStage.setWidth(400);
	 
	       primaryStage.show();
	   }
	 
	   public static void main(String[] args) {
	       Application.launch(args);
	   }
}
