import java.util.Random;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

	int[] numbers1;
	int[] numbers2;
	int[] numbers3;
	BarChart<String, Number> bubbleSortGraph;
	BarChart<String, Number> selectionSortGraph;
	BarChart<String, Number> insertionSortGraph;
	VBox bubbleHolder;
	VBox selectionHolder;
	VBox insertionHolder;
	
	@Override
	public void start(Stage primaryStage) {
        // Array generation and randomization
        numbers1 = generateRandomArray();
        numbers2 = numbers1.clone();
        numbers3 = numbers1.clone();
		
		StackPane root = new StackPane();
		
        primaryStage.setTitle("MultiThreading for Sorting Methods");
        primaryStage.setWidth(720);
        primaryStage.setHeight(1029);

		// Create a BarCharts
		XYChart.Series<String, Number> dataSeries1 = generateDataSeries(numbers1);
		XYChart.Series<String, Number> dataSeries2 = generateDataSeries(numbers2);
		XYChart.Series<String, Number> dataSeries3 = generateDataSeries(numbers3);

		selectionSortGraph = generateDataSeriesUpdateBarChart(numbers1, "Selection Sort", 0);
		insertionSortGraph = generateDataSeriesUpdateBarChart(numbers2, "Insertion Sort", 0);
		bubbleSortGraph = generateDataSeriesUpdateBarChart(numbers3, "Bubble Sort", 0);
        
		
		selectionSortGraph.getData().add(dataSeries1);
		selectionSortGraph.setTitle("Selection Sort");
		
		insertionSortGraph.getData().add(dataSeries2);
		insertionSortGraph.setTitle("Insertion Sort");
		
		bubbleSortGraph.getData().add(dataSeries3);
		bubbleSortGraph.setTitle("Bubble Sort");
		
		bubbleSortGraph.setAnimated(false);
		
		selectionHolder = new VBox();
		selectionHolder.getChildren().add(selectionSortGraph);
		
		
		insertionHolder = new VBox();
		insertionHolder.getChildren().add(insertionSortGraph);
		
		
		bubbleHolder = new VBox();
		bubbleHolder.getChildren().add(bubbleSortGraph);
		
	    Task<Void> selectionSortTask = new Task<Void>() {
	    	int length = numbers1.length;
	        @Override
	        protected Void call() {
	 	       for (int i = 0; i < length-1; i++) { 
		           int min_idx = i;
		           try {
			           for (int j = i+1; j < length; j++) {
			               if (numbers1[j] < numbers1[min_idx]) {
			                   min_idx = j;
			               }
			           }
			           int temp = numbers1[min_idx]; 
			           numbers1[min_idx] = numbers1[i]; 
			           numbers1[i] = temp;
			           int thisi = i;
	    		  		Platform.runLater(new Runnable() {
	                        @Override
	                        public void run() {
	    		    		  	BarChart<String, Number> selectionSortGraph = generateDataSeriesUpdateBarChart(numbers1, "Selection Sort", numbers1[thisi]);
	    		    		  	selectionHolder.getChildren().clear();
	    		    		  	selectionHolder.getChildren().add(selectionSortGraph); 	
	                        } 
                        });
		    		  	Thread.sleep(800);
		      	  } catch (InterruptedException ex) {}
	      	  }

			return null;
	        }
	    };
		
		
	    Task<Void> insertionSortTask = new Task<Void>() {
	    	int length = numbers2.length;
	    	@Override
	        protected Void call() {
	    		for(int j = 1; j < length; j++) {
	    			int key = numbers2[j];
	    			int i = j - 1;
	      		  try {
	      			while(i >= 0 && numbers2[i] > key) {
	      				numbers2[i + 1] = numbers2[i];
	    				i = i - 1;
		    		  }
	      			numbers2[i + 1] = key;
	      			int thisi = numbers2[i + 1];
	    		  		Platform.runLater(new Runnable() {
	                        @Override
	                        public void run() {
	    		    		  	BarChart<String, Number> insertionSortGraph = generateDataSeriesUpdateBarChart(numbers2, "Insertion Sort", thisi);
	    		    		  	insertionHolder.getChildren().clear();
	    		    		  	insertionHolder.getChildren().add(insertionSortGraph); 	
	                        } 
                        });
		    		  	Thread.sleep(800);
		      	  } catch (InterruptedException ex) {}
	      	  }

			return null;
	        }
	    };
        
        
	    Task<Void> bubbleSortTask = new Task<Void>() {
	    	int i = 0;
	    	int j = 0;
			int length = numbers3.length;
	        @Override
	        protected Void call() {
	      	  for( i = 0; i < length - 1; i++) {
	      		  try {
		    		  for(j = 0; j < length - i - 1; j++) {
	    				if(numbers3[j] > numbers3[j + 1]) {
	    					int temp = numbers3[j];
	    					numbers3[j] = numbers3[j + 1];
	    					numbers3[j + 1] = temp;
	    				}
		    		  }
		    		  int thisj = numbers3[i] ;
	    		  		Platform.runLater(new Runnable() {
	                        @Override
	                        public void run() {
	    		    		  	BarChart<String, Number> bubbleSortGraph = generateDataSeriesUpdateBarChart(numbers3, "Bubble Sort", thisj);
	    		    		  	bubbleHolder.getChildren().clear();
	    		    		  	bubbleHolder.getChildren().add(bubbleSortGraph); 	
	                        } 
                        });
		    		  	Thread.sleep(800);
		      	  } catch (InterruptedException ex) {}
	      	  }

			return null;
	        }
	    };
       
	    
	    Thread selectionSortThread = new Thread(selectionSortTask);
	    selectionSortThread.setDaemon(true);
	    selectionSortThread.start();
	    
	    Thread insertionSortThread = new Thread(insertionSortTask);
	    insertionSortThread.setDaemon(true);
	    insertionSortThread.start();
	    
	    Thread bubbleSortThread = new Thread(bubbleSortTask);
	    bubbleSortThread.setDaemon(true);
	    bubbleSortThread.start();     
        
        
        VBox vBox = new VBox(0);
        vBox.setPadding(new Insets(20));
        vBox.setAlignment(Pos.TOP_CENTER);
        vBox.getChildren().addAll(selectionHolder, insertionHolder, bubbleHolder);
        root.getChildren().add(vBox);
        
        Scene scene = new Scene(root, 500, 300);  
        
        primaryStage.setScene(scene);
        primaryStage.show();
        
	}
	
	
	public static BarChart<String, Number> generateDataSeriesUpdateBarChart(int[] numbers, String title, int barToColour) {
		CategoryAxis xAxis = new CategoryAxis();
		NumberAxis yAxis = new NumberAxis();
		BarChart<String, Number> barChart = new BarChart<String, Number>(xAxis, yAxis);
		barChart.setTitle(title);
		barChart.setAnimated(false);
		barChart.getYAxis().setOpacity(0);
		barChart.getXAxis().setOpacity(0);
		barChart.setLegendVisible(false);
		barChart.setCategoryGap(0.5);
		XYChart.Series<String, Number> dataSeries = new XYChart.Series<String, Number>();
		for(int i = 0; i < numbers.length; i++) {
			Data<String, Number> number = new XYChart.Data<String, Number>(Integer.toString(numbers[i]), numbers[i]);
			number.nodeProperty().addListener(new ChangeListener<Node>() {
				  @Override public void changed(ObservableValue<? extends Node> ov, Node oldNode, Node newNode) {
				    if (newNode != null) {
				      if (number.getYValue().intValue() == barToColour ) {
				        newNode.setStyle("-fx-bar-fill: navy;");
				      } else {
				        newNode.setStyle("-fx-bar-fill: firebrick;");
				      }  
				    }
				  }
			});
			dataSeries.getData().add(number);
		}
		
		barChart.getData().clear();
		barChart.getData().add(dataSeries);
		return barChart;
	}
	
	
	public static int[] generateRandomArray() {
        Random rand = new Random();
        int[] numbers = new int[30];
        for(int i = 0; i < 30; ++i) {
        	numbers[i] = i + 1;
        }
        
        for( int i = numbers.length - 1; i > 0; i--) {
        	// Shuffle array
        	int index = rand.nextInt(i + 1);
        	int temp = numbers[index];
        	numbers[index] = numbers[i];
        	numbers[i] = temp;
        }
        
        return numbers;
	}
	
	
	public static XYChart.Series<String, Number> generateDataSeries(int[] numbers) {
		XYChart.Series<String, Number> dataSeries = new XYChart.Series<String, Number>();
		for(int i = 0; i < numbers.length; i++) {
			Data<String, Number> number = new XYChart.Data<String, Number>(Integer.toString(numbers[i]), numbers[i]);
			dataSeries.getData().add(number);
		}
		return dataSeries;
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
	
}
