import java.sql.SQLException;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MainUi {

	public static void runUi(ObservableList<Student> oStudents, Stage primaryStage, boolean dataFromSearchResults) {
		TableView table = new TableView();
        StackPane root = new StackPane();
        
        primaryStage.setWidth(850);
        primaryStage.setHeight(500);
        
        // ----------------- SETUP TABLE --------------------------
        Label label = new Label("Student List");
        if(dataFromSearchResults) {
        	label.setText("Search Results");
        }
        label.setFont(new Font("Arial", 20));
 
        table.setEditable(true);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
 
        TableColumn studentIdCol = new TableColumn("Student ID");
        studentIdCol.setCellValueFactory(
                new PropertyValueFactory<Student, Integer>("studentId"));
        
        TableColumn nameCol = new TableColumn("Name");
        nameCol.setCellValueFactory(
                new PropertyValueFactory<Student, String>("name"));
        
        TableColumn quizCol = new TableColumn("Quiz");
        quizCol.setCellValueFactory(
                new PropertyValueFactory<Student, Double>("quiz"));
        
        TableColumn a1Col = new TableColumn("A1");
        a1Col.setCellValueFactory(
                new PropertyValueFactory<Student, Double>("a1"));
        
        TableColumn a2Col = new TableColumn("A2");
        a2Col.setCellValueFactory(
                new PropertyValueFactory<Student, Double>("a2"));
        
        TableColumn examCol = new TableColumn("Exam");
        examCol.setCellValueFactory(
                new PropertyValueFactory<Student, String>("exam"));
        
        TableColumn cumulativeMarkCol = new TableColumn("Cumulative Mark");
        cumulativeMarkCol.setCellValueFactory(
                new PropertyValueFactory<Student, Float>("cumulativeMark"));
        
        TableColumn gradeCol = new TableColumn("Grade");
        gradeCol.setCellValueFactory(
                new PropertyValueFactory<Student, String>("grade"));
        
        table.setItems(oStudents);
        table.getColumns().addAll(studentIdCol, nameCol, quizCol, a1Col, a2Col, examCol, cumulativeMarkCol, gradeCol);
        
        // --------------- END OF TABLE CREATION ----------------------------
 
        final VBox tableVBox = new VBox();
        tableVBox.setSpacing(5);
        tableVBox.setPadding(new Insets(10, 0, 0, 10));
        tableVBox.getChildren().addAll(label, table);
 
        
        // ------------------- HANDLE BUTTONS ------------------------------
        Button newStudent = new Button("Add Student");
        Button editStudent = new Button("Edit Student");
        Button searchStudent = new Button("Search Student");
        Button refresh = new Button("Refresh Table");
        Button exit = new Button("exit");
        
        
        newStudent.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
            	Student nullStudent = new Student(0, "null", 0, 0, 0, 0);
            	ConfigureStudentUi.runUi(refresh, nullStudent, true);
            }
        });
        
        
        exit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) { 
            	primaryStage.close();
            }
        });
        
        
        refresh.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) { 
        		ArrayList<Student> allStudents;
        		ObservableList<Student> oRefreshedAllStudents;
				try {
					allStudents = StudentController.getStudentRecords(true, null, null);
	        		oRefreshedAllStudents = FXCollections.observableArrayList(allStudents);
	        		table.setItems(oRefreshedAllStudents);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		        table.refresh();
            }
        });
        
        table.setOnMouseClicked((MouseEvent event) -> {
            int index = table.getSelectionModel().getSelectedIndex();
            Student student = (Student) table.getItems().get(index);            
            editStudent.setOnAction(new EventHandler<ActionEvent>() {
    			public void handle(ActionEvent arg0) {
    				System.out.println(student.getName());
    				ConfigureStudentUi.runUi(refresh, student, false);
    			}
            	
            });
        });
        
        
        editStudent.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent arg0) {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("No Student Selected");
				alert.setHeaderText("Error when attempting to edit student");
				alert.setContentText("A student must be selected in the table before the edit button is pressed");
				alert.showAndWait();
			}
        	
        });
        
        
        searchStudent.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent arg0) {
				SearchUi.runUi(primaryStage);
			}
        	
        });
       
        
        final VBox buttonVbox = new VBox();
        buttonVbox.setSpacing(15);
        buttonVbox.setPadding(new Insets(10, 0, 0, 10));
        buttonVbox.setAlignment(Pos.CENTER_LEFT);
        buttonVbox.getChildren().addAll(newStudent, editStudent, searchStudent, refresh, exit);
        
        // create a HBox to hold 2 VBoxes. There is one HBox for the buttons and the other is for the tableview
        HBox hbox = new HBox(30);
        HBox.setHgrow(buttonVbox, Priority.ALWAYS);
        hbox.setPadding(new Insets(20));
        hbox.getChildren().addAll(tableVBox, buttonVbox);
        root.getChildren().add(hbox);
        Scene scene = new Scene(root, 500, 300);      
      
        
        primaryStage.setTitle("Grade Processing");
        primaryStage.setScene(scene);
        primaryStage.show();
	}
}
