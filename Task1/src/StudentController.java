import java.util.ArrayList;

import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

import java.sql.*;


public class StudentController {
	
	private static String SQL_USER = "root";
	private static String SQL_PASSWORD = "password";
	private static String SQL_DB = "jdbc:mysql://localhost/gradprocessing";
	final static String TABLE_NAME = "java2";
	
	// --------------------- HANDLE DB Logic ------------------
	static public ArrayList<Student> getStudentRecords(boolean allStudents, String fieldToSearch, String value) throws SQLException, ClassNotFoundException {
		Connection con = setupDbConnection();
		if(!checkIfTableExists(con, TABLE_NAME)) {
        	createDbTable(con, TABLE_NAME);
        }
        
		ArrayList<Student> students = new ArrayList<Student>();
		String getStudents = "";
		if(allStudents) {
			getStudents = "SELECT * FROM " + TABLE_NAME;
		} else {
			if(fieldToSearch == "StudentID") {
				fieldToSearch = "ID";
			}
			getStudents = "SELECT * \r\n" + 
					"FROM gradprocessing.java2\r\n" + 
					"Where " + fieldToSearch + " Like '%" + value + "%';";
			System.out.println(getStudents);
		}
        ResultSet results = getDataFromDb(getStudents, con);
        
        while(results.next()) {
        	int studentId = Integer.parseInt(results.getString(1));
        	String name = results.getString(2);
        	double quiz = Double.parseDouble(results.getString(3));
        	double a1 = Double.parseDouble(results.getString(4));
        	double a2 = Double.parseDouble(results.getString(5));
        	double exam = Double.parseDouble(results.getString(6));
        	float cumulativeMark = Float.parseFloat(results.getString(7));
        	String grade = results.getString(8);
        	Student student = new Student(studentId, name, quiz, a1, a2, exam, cumulativeMark, grade);
        	students.add(student);
        }
        con.close();
		return students;
	}
	
	
	private static boolean checkIfTableExists(Connection con, String tableName) throws SQLException {
        DatabaseMetaData meta = con.getMetaData();
        ResultSet res = meta.getTables(null, null, tableName, 
           new String[] {"TABLE"});
        if(res.next()) {
        	return true;
        } else {
        	return false;
        }
	}
	
	private static void createDbTable(Connection con, String tableName) throws ClassNotFoundException, SQLException {
		String sql = "CREATE TABLE `gradprocessing`.`" + tableName + "` (\r\n" + 
				"  ID INT NOT NULL,\r\n" + 
				"  Name VARCHAR(100) NULL,\r\n" + 
				"  Quiz DOUBLE NULL,\r\n" + 
				"  A1 DOUBLE NULL,\r\n" + 
				"  A2 DOUBLE NULL,\r\n" + 
				"  Exam DOUBLE NULL,\r\n" + 
				"  CumulativeMark DOUBLE NULL,\r\n" + 
				"  Grade VARCHAR(45) NULL,\r\n" + 
				"  PRIMARY KEY ( ID )\r\n" + 
				");";
		pushDataToDb(sql, con);
	}
	
	
	static public void updateStudent(Student newStudent, Student studentToEdit) throws ClassNotFoundException, SQLException {
		Connection con = setupDbConnection();
		String sql = "UPDATE " + TABLE_NAME + "\r\n" + 
				"set\r\n" + 
				"	ID = " + newStudent.getStudentId() + ",\r\n" + 
				"	Name = \"" + newStudent.getName() + "\",\r\n" +
				"    Quiz = " + newStudent.getQuiz() + ",\r\n" +
				"    A1 = " + newStudent.getA1()+ ",\r\n" +
				"    A2 = " + newStudent.getA2() + ",\r\n" +
				"    Exam = " + newStudent.getExam() + ",\r\n" +
				"    CumulativeMark = " + newStudent.getCumulativeMark() + ",\r\n" +
				"    Grade = \"" + newStudent.getGrade() + "\"\r\n" +
				"where\r\n" + 
				"	ID = " + studentToEdit.getStudentId() + ";";
		System.out.println(sql);
		pushDataToDb(sql, con);
		con.close();
	}
	
	
	static public void addNewStudent(Student student) throws ClassNotFoundException, SQLException {
		Connection con = setupDbConnection();
		String sql = "INSERT INTO java2 (ID, Name, Quiz, A1, A2, Exam, CumulativeMark, Grade)\r\n";
		String newStudentQuery = student.getStudentId() + ", \"" + student.getName() + "\", " + student.getQuiz() + ", " + student.getA1() + ", " + student.getA2() + ", " + student.getExam() + ", " + student.getCumulativeMark()  + ", \"" + student.getGrade() + "\"";
		sql += "VALUES ("  + newStudentQuery + ")";
		pushDataToDb(sql, con);
		con.close();
	}
	
	
	static public boolean checkIfStudentAlreadyExists(Student student) throws ClassNotFoundException, SQLException {
		Connection con = setupDbConnection();
		String sql = "SELECT * FROM gradprocessing.java2 WHERE ID Like '" + student.getStudentId() + "';";
		ResultSet results = getDataFromDb(sql, con);
		if(results.next()) {
			con.close();
			return true;
		} else {
			con.close();
			return false;
		}
	}
	
	
	static private Connection setupDbConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection con = DriverManager.getConnection(SQL_DB, SQL_USER, SQL_PASSWORD);
		return con;
	}

	
	static private ResultSet getDataFromDb(String query, Connection conn) throws SQLException, ClassNotFoundException {
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        return resultSet;
	}

	
	static private void pushDataToDb(String query, Connection conn) throws SQLException, ClassNotFoundException {
        Statement statement = conn.createStatement();
        statement.execute(query);
	}
	
	
	// -------------- Handle UI Related Logic -----------------
    public static boolean validateMarkFields(ArrayList<TextField> requiredFields) {
    	for(TextField textField : requiredFields) {
    		if(textField.getText().isEmpty()) {
    			Alert alert = new Alert(AlertType.ERROR);
    			alert.setTitle("Calculate Marks Error");
    			alert.setHeaderText("Error with inputted student marks");
    			alert.setContentText("All mark values need to be inputed before final mark is calculated");
    			alert.showAndWait();
    			return false;
    		} else {
    			try {
    				double value = Double.parseDouble(textField.getText());
    				if(value < 0 || value > 100) {
            			for(TextField requiredTextField : requiredFields) {
            				requiredTextField.setText("");
            			}
    					displayMarkValueError();
    					return false;
    				}
    			} catch (Exception e){
        			for(TextField requiredTextField : requiredFields) {
        				requiredTextField.setText("");
        			}
    				displayMarkValueError();
        			return false;	
    			}
    		}
    	}
    	return true;
    }
    
    public static void displayMarkValueError() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Calculate Marks Error");
		alert.setHeaderText("Error with inputted student marks");
		alert.setContentText("All mark values need to be inputed as numbers between 0 and 100");
		alert.showAndWait();
    }
    
    public static boolean validateNewStudentFields(TextField studentIdField, TextField nameField, TextField quizMarksField, TextField a1Field, TextField a2Field, TextField examField) {
    	ArrayList<TextField> requiredFields = new ArrayList<TextField>();
    	requiredFields.add(studentIdField);
    	requiredFields.add(nameField);
    	requiredFields.add(quizMarksField);
    	requiredFields.add(a1Field);
    	requiredFields.add(a2Field);
    	requiredFields.add(examField);
    	
    	boolean result = validateStudentId(studentIdField);
    	if(!result) {
    		return true;
    	}
    	
    	for(TextField textField : requiredFields) {
    		if(textField.getText().isEmpty()) {
    			Alert alert = new Alert(AlertType.ERROR);
    			alert.setTitle("Add New Student Error");
    			alert.setHeaderText("Error with inputted student marks");
    			alert.setContentText("All fields need to be inputted before student is created");
    			alert.showAndWait();
    			for(TextField requiredTextField : requiredFields) {
    				requiredTextField.setText("");
    			}
    			return true;
    		}
    	}
    	return false;
    }
    
    public static boolean validateStudentId(TextField studentIdField) {
    	if(!(studentIdField.getText().length() == 8)) {
    		displayIdFieldError();
			return false;
    	}
		try {
			Integer.parseInt(studentIdField.getText());
		} catch (Exception e){
			displayIdFieldError();
			return false;
		}
    	return true;
    }
    
    public static void displayIdFieldError() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Add New Student Error");
		alert.setHeaderText("Error with inputted student ID");
		alert.setContentText("ID Field needs to be 8 numbers long and must not contain a string");
		alert.showAndWait();
    }
}
