import java.sql.SQLException;
import java.util.ArrayList;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class SearchUi {
	
	public static void runUi(Stage primaryStage) {
		//Parent root;
		Stage stage = new Stage();
		stage.setTitle("Add new student");
		Label newStudentLabel = new Label("Student Search");	
	    newStudentLabel.setFont(new Font("Arial", 20));
		
	    GridPane pane = new GridPane();
	    pane.add(newStudentLabel, 0, 0);
	    pane.setAlignment(Pos.CENTER);
	    pane.setHgap(10);
	    pane.setVgap(10);
	    Scene scene = new Scene(pane, 450, 200);
	    
	    ObservableList<String> options = FXCollections.observableArrayList(
	    	        "StudentID",
	    	        "Name",
	    	        "Quiz",
	    	        "A1",
	    	        "A2",
	    	        "Exam",
	    	        "CumulativeMark",
	    	        "Grade"
	    	    );
	    	final ComboBox searchField = new ComboBox(options);
	    searchField.setValue("StudentID");
	    
        Label studentIdLabel = new Label("Please select option to search with:");
        pane.add(studentIdLabel, 0, 1);
        pane.add(searchField, 0, 2);
        
        Label searchLabel = new Label("Enter value:");
        pane.add(searchLabel, 0, 3);
        TextField searchValue = new TextField();
        searchValue.setPromptText(searchField.getValue().toString());
        pane.add(searchValue, 1, 3);
        
        Button searchButton = new Button("Search");
        pane.add(searchButton, 0, 4);
        Button cancelButton = new Button("Cancel");
        pane.add(cancelButton, 1, 4);
        
        searchField.valueProperty().addListener(new ChangeListener<String>() {
        	@Override
        	public void changed(ObservableValue oc, String t, String t1) {
        		searchValue.setPromptText(searchField.getValue().toString());
        	}
        });
        
        searchButton.setOnAction(new EventHandler<ActionEvent>() {
        	public void handle(ActionEvent event) {
        		ArrayList<Student> allStudents;
        		if(searchValue.getText().isEmpty()) {
        			Alert alert = new Alert(AlertType.ERROR);
        			alert.setTitle("Search Student Error");
        			alert.setHeaderText("Error with Search Value");
        			alert.setContentText("No search value has been inputed. Please select a field to search by and enter a vlaue");
        			alert.showAndWait();
        			return;
        		}
				try {
					String searchByValue = searchValue.getText();
					String searchByField = searchField.getValue().toString();
					allStudents = StudentController.getStudentRecords(false, searchByField, searchByValue);
	        		ObservableList<Student> oAllStudents = FXCollections.observableArrayList(allStudents);
	        		stage.close();
	        		MainUi.runUi(oAllStudents, primaryStage, true);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        });
        
        
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
        	public void handle(ActionEvent event) {
        		stage.close();
        	}
        });
	    
		stage.setScene(scene);
		stage.show();	
	}
}
