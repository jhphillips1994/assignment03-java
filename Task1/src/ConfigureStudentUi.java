import java.sql.SQLException;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ConfigureStudentUi {
	
    public static void runUi(Button refreshButton, Student studentToEdit, boolean newStudent) {
    	Parent root;
    	Stage stage = new Stage();
    	Label newStudentLabel = new Label();	
        newStudentLabel.setFont(new Font("Arial", 20));
    	
        GridPane pane = new GridPane();
        pane.add(newStudentLabel, 0, 0);
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        
        Scene scene = new Scene(pane, 450, 425);
        
        Label studentIdLabel = new Label("Student ID:");
        pane.add(studentIdLabel, 0, 1);
        TextField studentIdField = new TextField();
        pane.add(studentIdField, 1, 1);
        
        Label nameLabel = new Label("Name:");
        pane.add(nameLabel, 0, 2);
        TextField nameField = new TextField();
        pane.add(nameField, 1, 2);		        
        
        Label marksLabel = new Label("Quiz Marks:");
        pane.add(marksLabel, 0, 3);        
        TextField quizMarksField = new TextField();
        pane.add(quizMarksField, 1, 3);
        
        Label a1Label = new Label("A1 Marks:");
        pane.add(a1Label, 0, 4);
        TextField a1Field = new TextField();
        pane.add(a1Field, 1, 4);
        
        Label a2Label = new Label("A2 Marks:");
        pane.add(a2Label, 0, 5);
        TextField a2Field = new TextField();
        pane.add(a2Field, 1, 5);
        
        Label examLabel = new Label("Exam Marks:");
        pane.add(examLabel, 0, 6);
        TextField examField = new TextField();
        pane.add(examField, 1, 6);
        
        
        Label cumulativeMarkLabel = new Label("Cumulative Marks:");
        pane.add(cumulativeMarkLabel, 0, 8);
        Label cumulativeMarkValue = new Label("");
        pane.add(cumulativeMarkValue, 1, 8);
        
        Label gradeLabel = new Label("Grade:");
        pane.add(gradeLabel, 0, 9);
        Label gradeValue = new Label("");
        pane.add(gradeValue, 1, 9);
        
        // Handle Buttons
        Button calculateMarkButton = new Button("Calculate Mark");
        HBox hbox = new HBox(10);
        hbox.setAlignment(Pos.BOTTOM_RIGHT);	        
        hbox.getChildren().add(calculateMarkButton);
        pane.add(hbox, 1, 7);
        
        Button saveNewStudent = new Button("Save New Student");        
        pane.add(saveNewStudent, 0, 10);
        
        Button cancelButton = new Button("Cancel");
        HBox hboxButton = new HBox(10);
        hboxButton.setAlignment(Pos.BOTTOM_RIGHT);
        
        hboxButton.getChildren().add(cancelButton);
        pane.add(cancelButton, 1, 10);
        
        
        // If the new Student value is flagged change the UI to generate new student screen
        if(newStudent) {
        	stage.setTitle("Add new student");
            newStudentLabel = new Label("Add new Student");
            studentIdField.setPromptText("Enter Student ID");
            nameField.setPromptText("Enter Name");
            quizMarksField.setPromptText("Marks should be between 0 and 100");
            a1Field.setPromptText("Marks should be between 0 and 100");
            a2Field.setPromptText("Marks should be between 0 and 100");
            examField.setPromptText("Marks should be between 0 and 100");
        } else {
        	stage.setTitle("Edit Student");
        	newStudentLabel = new Label("Edit Student");
        	studentIdField.setText(Integer.toString(studentToEdit.getStudentId()));
        	nameField.setText(studentToEdit.getName());
        	quizMarksField.setText(Double.toString(studentToEdit.getQuiz()));
        	a1Field.setText(Double.toString(studentToEdit.getA1()));
        	a2Field.setText(Double.toString(studentToEdit.getA2()));
        	examField.setText(Double.toString(studentToEdit.getExam()));
        	cumulativeMarkValue.setText(Float.toString(studentToEdit.getCumulativeMark()));
        	gradeValue.setText(studentToEdit.getGrade());
        	saveNewStudent.setText("Save Edited Student");
        }
    	
        
        calculateMarkButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
            	handleCalculateMarkButton(quizMarksField, a1Field, a2Field, examField, cumulativeMarkValue, gradeValue);
            }
        });
        
        
        saveNewStudent.setOnAction(new EventHandler<ActionEvent>() {
            
            public void handle(ActionEvent event) {
            	boolean result = StudentController.validateNewStudentFields(studentIdField, nameField, quizMarksField, a1Field, a2Field, examField);
            	if(result) {
            		return;
            	}
            	
            	boolean calculateMarkFailure = handleCalculateMarkButton(quizMarksField, a1Field, a2Field, examField, cumulativeMarkValue, gradeValue);
            	if(!calculateMarkFailure) {
            		return;
            	}
            	
            	int studentId = Integer.parseInt(studentIdField.getText());
            	String name = nameField.getText();
                Double quiz = Double.parseDouble(quizMarksField.getText());
                Double a1 = Double.parseDouble(a1Field.getText());
                Double a2 = Double.parseDouble(a2Field.getText());
                Double exam = Double.parseDouble(examField.getText());
                Student student = new Student(studentId, name, quiz, a1, a2, exam);
            	try {
            		if(newStudent) {
            			boolean studentAlreadyExists = StudentController.checkIfStudentAlreadyExists(student);
            			if(studentAlreadyExists) {
                			Alert alert = new Alert(AlertType.ERROR);
                			alert.setTitle("Add New Student Error");
                			alert.setHeaderText("Error with inputted student ID");
                			alert.setContentText("Student ID already exists in application. ID's must be unique");
                			alert.showAndWait();
                			return;
            			} else {
            				StudentController.addNewStudent(student);	
            			}	
            		} else {
            			StudentController.updateStudent(student, studentToEdit);
            		}
    			} catch (ClassNotFoundException e) {
    				e.printStackTrace();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
            	stage.close();
            	refreshButton.fire();
            }
        });
        
        
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) { 
            	stage.close();
            }
        });
        
    	stage.setScene(scene);
    	stage.show();
    }
    
    
    public static boolean handleCalculateMarkButton(TextField quizMarksField, TextField a1Field, TextField a2Field, TextField examField, Label cumulativeMarkValue, Label gradeValue) {
    	ArrayList<TextField> requiredFields = new ArrayList<TextField>();
    	requiredFields.add(quizMarksField);
    	requiredFields.add(a1Field);
    	requiredFields.add(a2Field);
    	requiredFields.add(examField);
    	
    	boolean result = StudentController.validateMarkFields(requiredFields);
    	if(!result) {
    		return false;
    	}
    	
        Double quiz = Double.parseDouble(quizMarksField.getText());
        Double a1 = Double.parseDouble(a1Field.getText());
        Double a2 = Double.parseDouble(a2Field.getText());
        Double exam = Double.parseDouble(examField.getText());
        Float cumulativeMark = Student.calculateCumulativeMark(quiz, a1, a2, exam);
        String grade = Student.calculateGrade(cumulativeMark);
        
        cumulativeMarkValue.setText(String.valueOf(cumulativeMark));
        gradeValue.setText(String.valueOf(grade));
        return true;
    }
}
