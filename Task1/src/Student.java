
public class Student {
	
	private int studentId;
	private String name;
	private double quiz;
	private double a1;
	private double a2;
	private double exam;
	private float cumulativeMark;
	private String grade;
	
	
	public Student(int studentId, String name, double quiz, double a1, double a2, double exam) {
		this.studentId = studentId;
		this.name = name;
		this.quiz = quiz;
		this.a1 = a1;
		this.a2 = a2;
		this.exam = exam;
		this.cumulativeMark = calculateCumulativeMark(quiz, a1, a2, exam);
		this.grade = calculateGrade(this.cumulativeMark);
	}
	
	
	public Student(int studentId, String name, double quiz, double a1, double a2, double exam, float cumulativeMark, String grade) {
		this.studentId = studentId;
		this.name = name;
		this.quiz = quiz;
		this.a1 = a1;
		this.a2 = a2;
		this.exam = exam;
		this.cumulativeMark = cumulativeMark;
		this.grade = grade;
	}
	
	
	public int getStudentId() {
		return studentId;
	}
	
	
	public String getName() {
		return name;
	}
	
	
	public double getQuiz() {
		return quiz;
	}
	
	
	public double getA1() {
		return a1;
	}
	
	
	public double getA2() {
		return a2;
	}
	
	
	public double getExam() {
		return exam;
	}
	
	
	public float getCumulativeMark() {
		return cumulativeMark;
	}
	
	
	public String getGrade() {
		return grade;
	}
	
	
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	

	public void setName(String name) {
		this.name = name;
	}
	
	
	public void setA1(double a1) {
		this.a1 = a1;
	}
	
	
	public void setA2(double a2) {
		this.a2 = a2;
	}
	
	
	public void setExam(double exam) {
		this.exam = exam;
	}
	
	
	public void setCumulativeMark(float cumulativeMark) {
		this.cumulativeMark = cumulativeMark;
	}
	
	
	public void setGrade(String grade) {
		this.grade = grade;
	}
	
	
	public static float calculateCumulativeMark(double quiz, double a1, double a2, double exam) {
		float result = (float) ((quiz * 0.05) + (a1 * 0.15) + (a2 * 0.2) + (exam * 0.6));
		return result;
	}
	
	public static String calculateGrade(float cumulativeMark) {
		String result = "";
		if(cumulativeMark < 50) {
			result = "FL";
		} else if(cumulativeMark >= 50 && cumulativeMark < 65) {
			result = "PS";
		} else if(cumulativeMark >= 65 && cumulativeMark < 75) {
			result = "CR";
		} else if(cumulativeMark >= 75 && cumulativeMark < 85) {
			result = "DI";
		} else if(cumulativeMark >= 85) {
			result = "HD";
		}
		return result;
	}
}
